import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      padding: const EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.thumb_up, 'Like'),
          _buildButtonColumn(color, Icons.thumb_down, 'Dislike'),
          _buildButtonColumn(color, Icons.send, 'Share'),
        ],
      ),
    );

    Widget pic1 = Container(
      padding: const EdgeInsets.all(5),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text('Hungry Cat', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('Doesnt he look hungry?', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
          // stuff here
        ],
      ),
    );

    Widget pic2 = Container(
      padding: const EdgeInsets.all(5),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text('Eating Cat', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('Doesnt he look full?', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
          // stuff here
        ],
      ),
    );

    Widget mainTitleSection = Container(
      padding: const EdgeInsets.all(5),
      child: Text(
        'These cats love fast food!',
        softWrap: true,
        style: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
        textAlign: TextAlign.center,
      ),
    );




    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('These cats love fast food!'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/cat1.png',
                width: 600,
                height: 200,
                fit: BoxFit.cover,
              ),
              pic1,
              buttonSection,
              Image.asset(
                'images/cat2.png',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              pic2,
              buttonSection
            ]
        ),
      ),
    ); // MaterialApp
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 5),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}